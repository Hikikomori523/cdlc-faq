---
layout: default
title: CDLC FAQ
permalink: /
---

# CDLC FAQ

_Last changed on: {{ page.last_modified_at | date: "%Y-%m-%d %H:%M" }}_


---

Test

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Getting Started](#getting-started)
  - [Installing CDLC](#installing-cdlc)
    - [PC]
    - [Mac]
    - [~~Xbox - Playstation (Depreciated)~~]
- [Tools for creating cdlc](#tools-for-creating-cdlc)
- [Other examples](#other-examples)
- [Troubleshooting](#troubleshooting)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Getting Started


### Installing CDLC

test cdlc

#### PC

test pc

#### Mac

test mac

#### ~~Xbox - Playstation (Depreciated)~~

test depreciated


## Tools for Creating CDLC

list tools

## Other examples

examples

## Troubleshooting

where to contact, discord, site, etc
